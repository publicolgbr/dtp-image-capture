const puppeteer = require('puppeteer');
const fs = require('fs').promises;

async function takeScreenShot() {
  console.time('take');
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    slowMo: 250,
    args: ['--proxy-server=us-pr.oxylabs.io:10000'],
  });
  const page = await browser.newPage();
  try {
    page.setViewport({ width: 1920, height: 1980 });
    await page.goto('https://obp.bancopichincha.co/', {
      waitUntil: 'networkidle0'      
    });
    await page.waitForSelector('#btn-login', { visible: true, timeout: 0 });
    await page.screenshot({ path: 'images/obp-bancopichincha-proxy.png' });
    let html = await page.content();
    await fs.writeFile('html/html-obp-bancopichincha-proxy.html',html);
  } catch (e) {
    throw new Error(e);
  } finally {
    await page.close();
    await browser.close();
  }
}

takeScreenShot()
  .then(() => {
    console.log(`\u{1F601} termino`);
    console.timeEnd('take');
  })
  .catch((err) => {
    console.error(`\u{1F62D} ${err.message}`);
  });
